<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>askolegacy</groupId>
    <artifactId>ezmoney</artifactId>
    <packaging>pom</packaging>
    <version>0.1</version>
    <modules>
        <module>common</module>
        <module>server</module>
        <module>client</module>
    </modules>

    <properties>
        <maven.surefire.plugin.version>2.21.0</maven.surefire.plugin.version>
        <maven.compiler.plugin.version>3.7.0</maven.compiler.plugin.version>

        <java.version>8</java.version>
        <project.sourceEncoding>UTF-8</project.sourceEncoding>

        <project.build.sourceEncoding>${project.sourceEncoding}</project.build.sourceEncoding>
        <project.reporting.outputEncoding>${project.sourceEncoding}</project.reporting.outputEncoding>

        <!-- Coverage configuration -->
        <jacoco.maven.version>0.8.1</jacoco.maven.version>

        <!-- Pitest Configuration-->
        <pitest.version>1.3.2</pitest.version>
        <pitest.junit5.version>0.4</pitest.junit5.version>

        <junit.vintage.version>5.1.1</junit.vintage.version>

        <xmlunit.core.version>2.6.0</xmlunit.core.version>
    </properties>

    <build>
        <pluginManagement>
            <plugins>
                <!-- Required for compiling the project usign maven -->
                <plugin><!-- Compiler configuration-->
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven.compiler.plugin.version}</version>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>

                        <showWarnings>true</showWarnings>   <!-- Needs this -->

                        <compilerArgs>
                            <arg>-Xlint:all</arg>   <!-- recommended -->
                        </compilerArgs>

                        <encoding>${project.build.sourceEncoding}</encoding>

                    </configuration>
                </plugin>

                <!-- Required for running unit tests -->
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven.surefire.plugin.version}</version>
                    <configuration>
                        <includes>
                            <include>**/Test*.java</include>
                            <include>**/*Test.java</include>
                            <include>**/*Tests.java</include>
                            <include>**/*TestCase.java</include>
                        </includes>
                        <excludes>
                            <exclude>**/*IT.java</exclude>
                        </excludes>
                        <!-- new configuration needed for coverage per test -->
                        <properties>
                            <property>
                                <name>listener</name>
                                <value>org.sonar.java.jacoco.JUnitListener</value>
                            </property>
                        </properties>
                    </configuration>
                    <dependencies>
                        <!-- This dependency must be included, otherwise Maven Surefire will not recognise the test cases -->
                        <dependency>
                            <groupId>org.junit.platform</groupId>
                            <artifactId>junit-platform-surefire-provider</artifactId>
                            <version>1.2.0</version>
                        </dependency>
                    </dependencies>
                </plugin>

                <!-- Required for generating coverage report -->
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco.maven.version}</version>
                    <executions>
                        <execution>
                            <id>default-prepare-agent</id>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>default-report</id>
                            <!--<phase>prepare-package</phase>-->
                            <goals>
                                <goal>report</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>default-check</id>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <rules> <!-- only used for jacoco:check goal https://www.eclemma.org/jacoco/trunk/doc/check-mojo.html -->
                            <rule>
                                <element>CLASS
                                </element> <!-- can be specified by BUNDLE, PACKAGE, CLASS, SOURCEFILE or METHOD -->
                                <excludes>
                                    <exclude>*Test</exclude> <!-- Exclude Test classes from coverage report -->
                                </excludes>
                                <limits>
                                    <!-- An example limit-->
                                    <!--<limit>
                                        <counter>LINE</counter>--> <!-- INSTRUCTION, LINE, BRANCH, COMPLEXITY, METHOD, CLASS -->
                                    <!--<value>COVEREDRATIO</value>--> <!-- can be specified by TOTALCOUNT, COVEREDCOUNT, MISSEDCOUNT, COVEREDRATIO, MISSEDRATIO -->
                                    <!--<minimum>0.50</minimum>-->
                                    <!--</limit>-->
                                </limits>
                            </rule>
                        </rules>
                    </configuration>
                </plugin>

                <!-- Required for generating PIT Mutation reports -->
                <plugin>
                    <groupId>org.pitest</groupId>
                    <artifactId>pitest-maven</artifactId>
                    <version>${pitest.version}</version>
                    <dependencies>
                        <dependency> <!-- Only required because PITest does not work with JUnit5 without it -->
                            <groupId>org.pitest</groupId>
                            <artifactId>pitest-junit5-plugin</artifactId>
                            <version>${pitest.junit5.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <outputFormats>
                            <outputFormat>XML</outputFormat>
                            <outputFormat>HTML</outputFormat>
                        </outputFormats>
                        <!--<verbose>true</verbose>-->
                    </configuration>
                </plugin>

                <!-- Build an executable JAR -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>3.1.0</version>
                    <configuration>
                        <archive>
                            <manifest>
                                <addClasspath>true</addClasspath>
                                <classpathPrefix>lib/</classpathPrefix>
                            </manifest>
                        </archive>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <dependencies>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>2.7</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.7</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>2.7</version>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>5.3.2</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.3.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>