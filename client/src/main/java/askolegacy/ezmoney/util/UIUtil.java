package askolegacy.ezmoney.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.stage.Stage;

import java.io.IOException;

public class UIUtil {

    public static FXMLLoader makeLoader(String uiFileName) {
        return new FXMLLoader(UIUtil.class.getResource(String.format("%s%s%s", "/fxml/", uiFileName, ".fxml")));
    }

    public static Stage getStageFromNode(Node node) {
        return (Stage) node.getScene().getWindow();
    }

    public static void closeStage(Node node) {
        getStageFromNode(node).close();
    }
}
