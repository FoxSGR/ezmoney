package askolegacy.ezmoney.config;

import askolegacy.ezmoney.util.InvalidValueException;
import askolegacy.ezmoney.util.NetworkUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClientConfigParser extends ConfigParser {

    public static final String USERNAME_IDENTIFIER = "username";
    public static final String SERVER_IP_IDENTIFIER = "serverip";

    private static final String DESCRIPTION = "Client Configuration File";

    public ClientConfigParser(String fileName) throws IOException {
        super(fileName, DESCRIPTION);
    }

    @Override
    protected List<ConfigNode> createConfigNodes() {
        List<ConfigNode> configNodes = new ArrayList<>();

        configNodes.add(new ConfigNode(USERNAME_IDENTIFIER, "Your username", ""));
        configNodes.add(new ConfigNode(SERVER_IP_IDENTIFIER, "The server's IP address", ""));
        configNodes.add(new ConfigNode(PORT_IDENTIFIER, "The server's port", 4444));

        return configNodes;
    }

    @Override
    protected void setNodeValues(Map<String, String> readConfig) throws ConfigEntryNotFoundException, InvalidValueException {
        parseUsername(readConfig);
        parseServerIP(readConfig);
        getNodeByID(PORT_IDENTIFIER).value = parsePort(readConfig);
    }

    private void parseUsername(Map<String, String> readConfig) throws ConfigEntryNotFoundException {
        ConfigNode usernameNode = getNodeByID(USERNAME_IDENTIFIER);

        validateKey(readConfig, usernameNode.key);
        usernameNode.value = readConfig.get(usernameNode.key);
    }

    private void parseServerIP(Map<String, String> readConfig) throws ConfigEntryNotFoundException, InvalidValueException {
        ConfigNode serverIPNode = getNodeByID(SERVER_IP_IDENTIFIER);

        validateKey(readConfig, serverIPNode.key);
        String ip = readConfig.get(serverIPNode.key);
        if (ip.isEmpty()) {
            serverIPNode.value = ip;
            return;
        }

        if (!NetworkUtil.isIPValid(ip)) {
            throw new InvalidValueException("The server ip specifier in the config file is not a valid IPv4 address");
        }
        serverIPNode.value = ip;
    }
}
