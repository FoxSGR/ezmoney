package askolegacy.ezmoney.ui;

import askolegacy.ezmoney.util.Config;
import javafx.scene.control.Alert;

public class DialogBuilder {

    private DialogBuilder() {
        // Private constructor to hide implicit public one.
    }

    public static void showError(String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(Config.APP_NAME + " - Error");
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
}
