package askolegacy.ezmoney.ui;

import askolegacy.ezmoney.config.ClientConfigParser;
import askolegacy.ezmoney.core.Client;
import askolegacy.ezmoney.util.Config;
import askolegacy.ezmoney.util.UIUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class ClientMain extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientMain.class);

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Map<String, Object> config = parseConfigs();
        if (config == null) {
            return;
        }

        String username = (String) config.get(ClientConfigParser.USERNAME_IDENTIFIER);
        String serverIP = (String) config.get(ClientConfigParser.SERVER_IP_IDENTIFIER);
        int port = (int) config.get(ClientConfigParser.PORT_IDENTIFIER);

        Client client = new Client();

        startUI(primaryStage, client, username, serverIP, port);
    }

    private void startUI(Stage primaryStage, Client client, String username, String serverIP, int port) {
        LOGGER.info("Loading UI");

        primaryStage.setTitle(Config.APP_NAME);
        FXMLLoader rootLoader = UIUtil.makeLoader("RootUI");

        Parent rootNode = loadRootNode(rootLoader);
        if (rootNode == null) {
            return;
        }

        RootUI rootUI = rootLoader.getController();
        rootUI.setClient(client);
        client.setRootUI(rootUI);

        LoginUI loginUI = (LoginUI) rootUI.createNode(LoginUI.IDENTIFIER);
        loginUI.setUsernameFieldText(username);

        rootUI.createNode(SignUpUI.IDENTIFIER);

        ConnectUI connectUI = (ConnectUI) rootUI.createNode(ConnectUI.IDENTIFIER);
        connectUI.setConnectionData(serverIP, port);

        rootUI.changeUI(ConnectUI.IDENTIFIER);

        if (!serverIP.isEmpty()) {
            client.connect(serverIP, port);
        }

        primaryStage.setScene(new Scene(rootNode));
        primaryStage.show();
    }

    private static Map<String, Object> parseConfigs() {
        try {
            return new ClientConfigParser("clientconfig.txt").parseConfig();
        } catch (IOException e) {
            DialogBuilder.showError("Error while trying to read the configuration file.", e.getMessage());
            return null;
        }
    }

    private static Parent loadRootNode(FXMLLoader loader) {
        try {
            return loader.load();
        } catch (IOException e) {
            DialogBuilder.showError("Error while trying to read an FXML file.", "Please report this.");
            return null;
        }
    }
}
