package askolegacy.ezmoney.ui;

import askolegacy.ezmoney.util.Config;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ConnectUI extends ChildScene implements Initializable {

    @FXML
    private Label applicationName;

    @FXML
    private TextField ipField;

    @FXML
    private TextField portField;

    public static final String IDENTIFIER = "ConnectUI";

    public void setConnectionData(String ip, int port) {
        ipField.setText(ip);
        portField.setText(String.valueOf(port));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        applicationName.setText(Config.APP_NAME);
    }

    @FXML
    private void connectAction(ActionEvent actionEvent) {
        final String invalidPortHeader = "Invalid port";

        String ip = ipField.getText();
        int port;
        try {
            port = Integer.parseInt(portField.getText());
        } catch (NumberFormatException e) {
            DialogBuilder.showError(invalidPortHeader, "The port is not an integer");
            return;
        }

        client.connect(ip, port);
    }
}
