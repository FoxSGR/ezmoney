package askolegacy.ezmoney.ui;

import askolegacy.ezmoney.model.Profile;
import askolegacy.ezmoney.util.Config;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginUI extends ChildScene implements Initializable {

    public static final String IDENTIFIER = "LoginUI";

    @FXML
    private Label applicationName;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    private Profile profile;

    public void setUsernameFieldText(String username) {
        usernameField.setText(username);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        applicationName.setText(Config.APP_NAME);
    }

    @FXML
    private void signUpAction(ActionEvent actionEvent) {
        rootUI.changeUI(SignUpUI.IDENTIFIER);
    }

    @FXML
    private void loginAction(ActionEvent actionEvent) {
        // todo
    }
}
