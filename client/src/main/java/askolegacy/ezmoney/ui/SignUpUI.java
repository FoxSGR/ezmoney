package askolegacy.ezmoney.ui;

import askolegacy.ezmoney.util.Config;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class SignUpUI extends ChildScene implements Initializable{

    @FXML
    private Label applicationName;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    static final String IDENTIFIER = "SignUpUI";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        applicationName.setText(Config.APP_NAME);
    }

    @FXML
    public void signUpAction(ActionEvent actionEvent) {
        // todo
    }

    @FXML
    public void loginAction(ActionEvent actionEvent) {
        rootUI.changeUI(LoginUI.IDENTIFIER);
    }
}
