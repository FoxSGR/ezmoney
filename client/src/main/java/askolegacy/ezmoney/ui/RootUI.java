package askolegacy.ezmoney.ui;

import askolegacy.ezmoney.util.UIUtil;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.*;

public class RootUI extends UIController implements Initializable {

    @FXML
    private StackPane stackPane;

    @FXML
    private Label footer;

    private FadeTransition fadeIn;
    private Queue<FooterMessage> footerMessageQueue;
    private boolean footerMessageShowing;
    private List<Node> nodes;
    private Socket socket;

    private static final double FOOTER_FADE_IN_DURATION = 0.3;
    private static final double FOOTER_DURATION = 2;
    private static final double FOOTER_FADE_OUT_DURATION = 0.3;

    private class FooterMessage {

        String text;
        String color;

        private FooterMessage(String text, String color) {
            this.text = text;
            this.color = color;
        }
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        footerMessageShowing = false;
        footerMessageQueue = new LinkedList<>();
        nodes = new ArrayList<>();

        setupFadeTransitions();
    }

    ChildScene createNode(String identifier) {
        try {
            FXMLLoader loader = UIUtil.makeLoader(identifier);
            Node node = loader.load();
            node.setUserData(identifier);

            ChildScene controller = loader.getController();
            controller.setRootUI(this);
            controller.setClient(client);

            nodes.add(node);

            return controller;
        } catch (IOException e) {
            DialogBuilder.showError("Error while trying to load UIs.", "Please report this.");
            return null;
        }
    }

    public void showFooterMessage(String text, String color) {
        FooterMessage message = new FooterMessage(text, color);

        if (footerMessageShowing) {
            footerMessageQueue.add(message);
        } else {
            playFooterAnimation(message);
        }
    }

    public void changeUI(String identifier) {
        for (Node node : nodes) {
            String nodeID = (String) node.getUserData();
            if (nodeID.equalsIgnoreCase(identifier)) {
                changeUI(node);
            }
        }
    }

    void clearNodes(String... identifiers) {
        for (String id : identifiers) {
            for (Node node : nodes) {
                String nodeID = (String) node.getUserData();
                if (nodeID.equalsIgnoreCase(id)) {
                    nodes.remove(node);
                }
            }
        }
    }

    void clearNodes() {
        nodes.clear();
    }

    private void setupFadeTransitions() {
        FadeTransition fadeOut = new FadeTransition(Duration.seconds(FOOTER_FADE_OUT_DURATION), footer);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.setOnFinished(event -> {
            if (!footerMessageQueue.isEmpty()) {
                playFooterAnimation(footerMessageQueue.remove());
                return;
            }

            footerMessageShowing = false;
        });

        FadeTransition fadeMiddle = new FadeTransition(Duration.seconds(FOOTER_DURATION), footer);
        fadeMiddle.setFromValue(1.0);
        fadeMiddle.setToValue(1.0);
        fadeMiddle.setOnFinished(event -> fadeOut.playFromStart());

        fadeIn = new FadeTransition(Duration.seconds(FOOTER_FADE_IN_DURATION), footer);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setOnFinished(event -> fadeMiddle.playFromStart());
    }

    private void playFooterAnimation(FooterMessage message) {
        footerMessageShowing = true;

        footer.setText(message.text);
        footer.setTextFill(Color.web(message.color, 1));
        fadeIn.playFromStart();
    }

    private void changeUI(Node node) {
        stackPane.getChildren().setAll(node);
        StackPane.setAlignment(node, Pos.CENTER);
    }
}
