package askolegacy.ezmoney.core;

import askolegacy.ezmoney.model.Profile;
import askolegacy.ezmoney.ui.DialogBuilder;
import askolegacy.ezmoney.ui.LoginUI;
import askolegacy.ezmoney.ui.RootUI;
import askolegacy.ezmoney.util.ThreadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Client {

    private RootUI rootUI;

    private Socket socket;
    private Profile profile;

    private final Queue<String> outgoingQueue;

    private static final long RECHECK_INPUT_DELAY = 50;
    private static final int MAX_CONNECTION_ATTEMPS = 3;
    private static final int FIRST_CONNECTION_ATTEMPT_DELAY = 5;
    private static final int CONNECTION_ATTEMPT_DELAY = 10;

    private static final Logger LOGGER = LoggerFactory.getLogger(Client.class);

    public Client() {
        outgoingQueue = new LinkedList<>();
    }

    public void setRootUI(RootUI rootUI) {
        this.rootUI = rootUI;
    }

    public void connect(String ip, int port) {
        try {
            socket = new Socket(ip, port);
            rootUI.changeUI(LoginUI.IDENTIFIER);
            rootUI.showFooterMessage("Connected", "#21ff00");

            LOGGER.info("Connected to {}:{}", ip, port);
        } catch (IOException e) {
            DialogBuilder.showError("Could not connect to the server.", "Error message:\n" + e.getMessage());
        }
    }

    public void listen() {
        listen(1);
    }

    private void listen(int attempt) {
        LOGGER.info("Starting the server handler");

        try (PrintWriter outgoing = new PrintWriter(socket.getOutputStream(), false);
             InputStream inputStream = socket.getInputStream();
             Scanner incoming = new Scanner(inputStream)) {
            outgoing.flush();
            while (!socket.isClosed()) {
                if (inputStream.available() > 0) {
                    System.out.println(incoming.nextLine());
                }

                if (outgoingQueue.isEmpty()) {
                    outgoing.append(outgoingQueue.remove());
                    outgoing.flush();
                }

                ThreadUtil.waitForMS(RECHECK_INPUT_DELAY);
            }
            LOGGER.info("Server connection finished");
        } catch (IOException e) {
            if (attempt > MAX_CONNECTION_ATTEMPS) {
                LOGGER.error("Giving up after %s attempts");
                return;
            }

            int delay = CONNECTION_ATTEMPT_DELAY;
            if (attempt == 1) {
                delay = FIRST_CONNECTION_ATTEMPT_DELAY;
            }

            LOGGER.error("Error while communicating with server");
        }
    }
}
