package askolegacy.ezmoney.util;

import askolegacy.ezmoney.model.Profile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;

public class UserDataManagerTest {

    private static final String VALID_USERNAME = "test";
    private static final String VALID_PASSWORD = "a_password";
    private static final String TEST_FILE_NAME = "db_test";

    @Test
    void ensureRegisteringUserWorks() throws InvalidCredentialException {
        deleteOldFile();

        UserDataManager udm = new UserDataManager(TEST_FILE_NAME);
        udm.register(VALID_USERNAME, VALID_PASSWORD);
        // If no exception is thrown the test passed
    }

    @Test
    void ensureAuthenticatingWorks() throws InvalidCredentialException {
        deleteOldFile();

        Profile expected = new Profile(VALID_USERNAME, UserDataManager.getInitialBalance());
        Profile result = registerAndAuthenticate(VALID_USERNAME, VALID_PASSWORD, VALID_USERNAME, VALID_PASSWORD);

        Assertions.assertEquals(expected, result);
    }

    @Test
    void ensureWrongPasswordFailsAuth() throws InvalidCredentialException {
        deleteOldFile();
        Profile result = registerAndAuthenticate(VALID_USERNAME, VALID_PASSWORD, VALID_USERNAME, VALID_PASSWORD + "aa");
        Assertions.assertNull(result);
    }

    @Test
    void ensureWrongUsernameFailsAuth() throws InvalidCredentialException {
        deleteOldFile();

        Profile expected = null;
        Profile result = registerAndAuthenticate(VALID_USERNAME, VALID_PASSWORD, VALID_USERNAME + "aa", VALID_PASSWORD);

        Assertions.assertEquals(expected, result);
    }

    @Test
    void ensureAuthWithNoUsersFails() {
        deleteOldFile();

        Profile expected = null;
        Profile result = new UserDataManager(TEST_FILE_NAME).authenticate(VALID_USERNAME, VALID_PASSWORD);

        Assertions.assertEquals(expected, result);
    }

    @Test
    void ensureBannedCharsAreNotAllowed() {
        deleteOldFile();

        String baseUsername = "test";
        String basePassword = "a_password";

        testCredentialChars(baseUsername, true);
        testCredentialChars(basePassword, false);
    }

    private static Profile registerAndAuthenticate(String registerUsername, String registerPassword, String loginUsername, String loginPassword) throws InvalidCredentialException {
        UserDataManager udm = new UserDataManager(TEST_FILE_NAME);
        udm.register(registerUsername, registerPassword);
        return udm.authenticate(loginUsername, loginPassword);
    }

    private static void testCredentialChars(String baseCredential, boolean username) {
        UserDataManager udm = new UserDataManager(TEST_FILE_NAME);

        for (char c : UserDataManager.getBannedChars()) {
            try {
                if (username) {
                    udm.register(baseCredential + c, VALID_PASSWORD);
                } else {
                    udm.register(VALID_USERNAME, baseCredential + c);
                }
                Assertions.fail("InvalidCredentialException not thrown when testing char " + c);
            } catch (InvalidCredentialException e) {
                // Ignore
            }
        }
    }

    private static void deleteOldFile() {
        File file = new File(TEST_FILE_NAME);
        if (file.isFile() && !file.delete()) {
            Assertions.fail("Unable to delete file " + TEST_FILE_NAME);
        }
    }
}
