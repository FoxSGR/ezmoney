package askolegacy;

import askolegacy.ezmoney.core.Server;
import askolegacy.ezmoney.menu.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ServerMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerMain.class);

    public static void main(String[] args) {
        Thread.currentThread().setName("MAIN");

        try {
            Server server = new Server();
            new Thread(server::start, "SERV").start();

            Menu menu = new Menu(server);
            menu.start();
        } catch (IOException e) {
            LOGGER.error("Couldn't start the server! Reason: {}", e.getMessage());
        }
    }
}
