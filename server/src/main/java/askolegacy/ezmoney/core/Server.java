package askolegacy.ezmoney.core;

import askolegacy.ezmoney.config.ConfigParser;
import askolegacy.ezmoney.config.ServerConfigParser;
import askolegacy.ezmoney.connection.Reply;
import askolegacy.ezmoney.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class Server {

    private ServerSocket serverSocket;
    private List<ClientConnection> clients;
    private final UserDataManager userDataManager;
    private boolean debugMode = true;
    private boolean running;

    private static final int MAX_CLIENTS = 30;
    private static final int LISTEN_DELAY = 50;
    private static final int UPDATE_DELAY = 200;
    private static final String UDM_FILE_NAME = "DB";

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    public Server() throws IOException {
        running = false;
        parseConfigs();
        userDataManager = new UserDataManager(UDM_FILE_NAME);
    }

    public boolean isRunning() {
        return running;
    }

    public void start() {
        LOGGER.info("Server bound to {}:{}", serverSocket.getInetAddress().getHostAddress(), serverSocket.getLocalPort());
        clients = new LinkedList<>();
        running = true;
        new Thread(this::update, "UPDT").start();
        LOGGER.debug("Starting listening");
        listen();
    }

    public List<String> getIPs() {
        List<String> ips = new ArrayList<>();
        for (ClientConnection thread : clients) {
            ips.add(thread.getIP());
        }
        return ips;
    }

    public void stop() {
        running = false;
        Iterator<ClientConnection> iterator = clients.iterator();
        while (iterator.hasNext()) {
            ClientConnection client = iterator.next();
            try {
                client.disconnect();
                iterator.remove();
            } catch (IOException e) {
                LOGGER.warn("Unable to disconnect client with IP \"{}\"", client.getIP());
            }
        }
        try {
            serverSocket.close();
        } catch (IOException e) {
            LOGGER.warn("Couldn't stop server socket!");
        }
        LOGGER.info("Server stopped.");
    }

    public void kickByIP(String ipAddress) {
        boolean found = false;
        for (ClientConnection client : clients) {
            String currentIP = client.getIP().replace("/", "");
            String portPart = ":" + currentIP.split(":")[1];
            if (currentIP.replace(portPart, "").equals(ipAddress)) {
                found = true;
                disconnectClient(client);
                break;
            }
        }
        if (!found) {
            LOGGER.info("No client is connected with that IP address.");
        }
    }

    private void update() {
        LOGGER.debug("Starting update thread");
        while (running) {
            long initialTime = System.currentTimeMillis();
            checkClientConnections();
            long finalTime = System.currentTimeMillis();
            ThreadUtil.waitForMS(LogicUtil.timeToWait(UPDATE_DELAY, initialTime, finalTime));
        }
    }

    private void checkClientConnections() {
        for (ClientConnection client : clients) {
            if (client.shouldDestroy()) {
                disconnectClient(client);
            }
        }
    }

    private void disconnectClient(ClientConnection client) {
        try {
            client.disconnect();
            clients.remove(client);
        } catch (IOException e) {
            LOGGER.warn("Unable to disconnect client with IP \"{}\"", client.getIP());
        }
    }

    private void listen() {
        LOGGER.info("Listening...");
        while (running) {
            long initialTime = System.currentTimeMillis();
            try {
                Socket socket = serverSocket.accept();
                if (clients.size() < MAX_CLIENTS) {
                    handleNewClient(socket);
                } else {
                    informFull(socket);
                    socket.close();
                }
                long finalTime = System.currentTimeMillis();
                ThreadUtil.waitForMS(LogicUtil.timeToWait(LISTEN_DELAY, initialTime, finalTime));
            } catch (IOException e) {
                if (running) {
                    LOGGER.warn("Failed to accept connection.");
                }
            }
        }
    }

    private void handleNewClient(Socket socket) {
        for (ClientConnection client : clients) {
            if (socket.getRemoteSocketAddress().toString().equals(client.getIP())) {
                LOGGER.info("Client tried to connect from same ip and port \"{}\"", client.getIP());
                return;
            }
        }

        LOGGER.info("Accepted connection from \"{}\"", socket.getRemoteSocketAddress());
        ClientConnection newConnection = ClientConnection.createConnection(socket, debugMode);
        new Thread(newConnection::start, String.format("client-%s", newConnection.getIP())).start();
        clients.add(newConnection);
    }

    private void informFull(Socket socket) {
        String ip = socket.getRemoteSocketAddress().toString();

        LOGGER.info("Rejected to add client from \"{}\" because the server is full ({} clients).", ip, clients.size());
        try {
            PrintWriter outgoing = new PrintWriter(socket.getOutputStream());
            outgoing.write(Reply.SERVER_FULL.toString());
            outgoing.flush();
        } catch (IOException e) {
            LOGGER.warn("Failed to inform the client that the server is full.");
        }
    }

    private void parseConfigs() throws IOException {
        ConfigParser configParser = new ServerConfigParser("serverconfig.txt");
        Map<String, Object> config = configParser.parseConfig();
        int port = (int) config.get(ConfigParser.PORT_IDENTIFIER);
        serverSocket = new ServerSocket(port, 1, InetAddress.getLocalHost());
        debugMode = (boolean) config.get(ServerConfigParser.DEBUG_MODE_IDENTIFIER);
    }
}
