package askolegacy.ezmoney.core;

import askolegacy.ezmoney.model.Profile;
import askolegacy.ezmoney.util.ThreadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientConnection {

    private boolean debugMode;
    private boolean alive;
    private long lastTimeConnected;
    private boolean keepAliveUndergoing;
    private Profile profile;
    private final Socket socket;

    private static final int KEEP_ALIVE_START_WAIT = 60 * 1000;
    private static final int KEEP_ALIVE_TIME = 30 * 1000;
    private static final int RECHECK_INPUT_DELAY = 50;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientConnection.class);

    private ClientConnection(Socket socket, boolean debugMode) {
        this.socket = socket;
        this.debugMode = debugMode;
        alive = true;
        keepAliveUndergoing = false;
        updateLastTimeConnected();
    }

    public String getIP() {
        return socket.getRemoteSocketAddress().toString();
    }

    public void start() {
        try (PrintWriter outgoing = new PrintWriter(socket.getOutputStream());
             Scanner incoming = new Scanner(socket.getInputStream())) {
            while (!socket.isClosed()) {
                if (incoming.hasNextLine()) {
                    if (keepAliveUndergoing) {
                        LOGGER.debug("Keep alive check passed for client with IP \"{}\"", getIP());
                        keepAliveUndergoing = false;
                    }
                    String input = incoming.nextLine();
                    String reply = input + " recebido broda";
                    outgoing.write(reply + "\n");
                    outgoing.flush();

                    LOGGER.debug("Client \"{}\" sent \"{}\"; Replied with \"{}\"", getIP(), input, reply);
                    updateLastTimeConnected();
                }
                ThreadUtil.waitForMS(RECHECK_INPUT_DELAY);
            }
        } catch (IOException e) {
            LOGGER.error("Error trying to reply to \"{}\": {}", socket.getRemoteSocketAddress(), e.getMessage());
        }

        if (alive) {
            logConnectionLoss();
            alive = false;
        }
    }

    public boolean shouldDestroy() {
        final long delta = System.currentTimeMillis() - lastTimeConnected;

        if (keepAliveUndergoing && delta > KEEP_ALIVE_START_WAIT + KEEP_ALIVE_TIME) {
            try {
                socket.close();
                LOGGER.debug("Keep alive check timed out for client \"{}\"", getIP());
                return true;
            } catch (IOException e) {
                LOGGER.error("Could not close timed out socket. Client IP: \"{}\"", getIP());
                return false;
            }
        }

        if (delta > KEEP_ALIVE_START_WAIT && !keepAliveUndergoing) {
            LOGGER.debug("Starting keep alive check for client \"{}\"", getIP());
            keepAliveUndergoing = true;
        }
        return !alive;
    }

    public void disconnect() throws IOException {
        if (!socket.isClosed()) {
            socket.close();
        }

        if (alive) {
            logConnectionLoss();
            alive = false;
        }
    }

    public static ClientConnection createConnection(Socket socket, boolean debugMode) {
        ClientConnection connection = new ClientConnection(socket, debugMode);
        connection.start();
        return connection;
    }

    private void logConnectionLoss() {
        LOGGER.info("Client with IP \"{}\" disconnected", getIP());
    }

    private void updateLastTimeConnected() {
        lastTimeConnected = System.currentTimeMillis();
    }
}
