package askolegacy.ezmoney.util;

public class InvalidCredentialException extends Exception {

    private static final long serialVersionUID = 1;

    public InvalidCredentialException(String credentialIdentifier, int minSize, int maxSize) {
        super(String.format("The %s must be between %d and %d characters long", credentialIdentifier, minSize, maxSize));
    }

    public InvalidCredentialException(String credentialIdentifier, char invalidChar) {
        super(String.format("The %s cannot contain the following character: %c", credentialIdentifier,  invalidChar));
    }
}
