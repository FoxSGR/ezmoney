package askolegacy.ezmoney.util;

import askolegacy.ezmoney.model.Profile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UserDataManager {

    private final String fileName;

    private static final int INITIAL_BALANCE = 200;
    private static final char[] BANNED_CHARS = {'`', '´', '«', '»'};

    private static final String SEPARATOR = "```";
    private static final int USERNAME_POSITION = 0;
    private static final int PASSWORD_POSITION = 1;
    private static final int BALANCE_POSITION = 2;

    private static final int USERNAME_MINIMUM_LENGTH = 3;
    private static final int USERNAME_MAXIMUM_LENGTH = 10;
    private static final int PASSWORD_MINIMUM_LENGTH = 6;
    private static final int PASSWORD_MAXIMUM_LENGTH = 16;

    public UserDataManager(String fileName) {
        handleFile(fileName);
        this.fileName = fileName;
    }

    public Profile authenticate(String username, String password) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                String[] parts = scanner.nextLine().split(SEPARATOR);
                if (parts[USERNAME_POSITION].equalsIgnoreCase(username) && hash(password).equals(parts[PASSWORD_POSITION])) {
                    return new Profile(username, Double.parseDouble(parts[BALANCE_POSITION]));
                }
            }
        } catch (FileNotFoundException e) {
            handleFileDisappearing();
        }
        return null;
    }

    public void register(String username, String password) throws InvalidCredentialException {
        validateCredential(username, "username", USERNAME_MINIMUM_LENGTH, USERNAME_MAXIMUM_LENGTH);
        validateCredential(password, "password", PASSWORD_MINIMUM_LENGTH, PASSWORD_MAXIMUM_LENGTH);

        Map<Integer, String> positionMap = new HashMap<>();
        positionMap.put(0, "´´´");
        positionMap.put(1, "«««");
        positionMap.put(2, "»»»");

        String newLine = String.format(
                "%s%s%s%s%s%n",
                positionMap.get(0),
                SEPARATOR,
                positionMap.get(1),
                SEPARATOR,
                positionMap.get(2)
        );

        newLine = newLine.replace(positionMap.get(USERNAME_POSITION), username)
                .replace(positionMap.get(PASSWORD_POSITION), hash(password))
                .replace(positionMap.get(BALANCE_POSITION), String.valueOf(INITIAL_BALANCE));

        try (PrintWriter writer = new PrintWriter(new File(fileName))) {
            writer.write(newLine);
        } catch (FileNotFoundException e) {
            handleFileDisappearing();
        }
    }

    private void handleFileDisappearing() {
        throw new InternalException("User data manager file (" + fileName + ") disappeared");
    }

    public static int getInitialBalance() {
        return INITIAL_BALANCE;
    }

    public static char[] getBannedChars() {
        return BANNED_CHARS;
    }

    private static String hash(String content) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(content.getBytes());
            return new String(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new InternalException("Unknown algorithm");
        }
    }

    private static void handleFile(String fileName) {
        File udmFile = new File(fileName);
        if (!udmFile.isFile()) {
            try {
                if (!udmFile.createNewFile()) {
                    throw new IOException(); // So it goes in the catch clause below
                }
            } catch (IOException e) {
                throw new InternalException("Unable to create user data manager file");
            }
        }
    }

    private static void validateCredential(String credential, String credentialIdentifier, int minSize, int maxSize) throws InvalidCredentialException {
        validateCredentialCharacters(credential, credentialIdentifier);
        validateCredentialLength(credential, credentialIdentifier, minSize, maxSize);
    }

    private static void validateCredentialCharacters(String credential, String credentialIdentifier) throws InvalidCredentialException {
        for (char c : credential.toCharArray()) {
            if (ArrayUtil.arrayContains(BANNED_CHARS, c)) {
                throw new InvalidCredentialException(credentialIdentifier, c);
            }
        }
    }

    private static void validateCredentialLength(String credential, String credentialIdentifier, int minSize, int maxSize) throws InvalidCredentialException {
        int credentialLength = credential.length();
        if (credentialLength > maxSize || credentialLength < minSize) {
            throw new InvalidCredentialException(credentialIdentifier, minSize, maxSize);
        }
    }
}
