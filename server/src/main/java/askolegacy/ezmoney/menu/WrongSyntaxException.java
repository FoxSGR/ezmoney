package askolegacy.ezmoney.menu;

public class WrongSyntaxException extends Exception {

    private static final long serialVersionUID = 1;

    public WrongSyntaxException(String message) {
        super(message);
    }
}
