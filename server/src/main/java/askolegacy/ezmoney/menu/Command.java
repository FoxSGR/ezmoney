package askolegacy.ezmoney.menu;

import askolegacy.ezmoney.core.Server;
import askolegacy.ezmoney.util.ListUtil;
import org.slf4j.Logger;

import java.util.Arrays;
import java.util.Comparator;

public enum Command implements ICommand {

    STOP {
        @Override
        public String toString() {
            return "stop";
        }

        @Override
        public void execute(String[] inputParts, Server server) {
            server.stop();
        }

        @Override
        public String description() {
            return "Stops the server.";
        }
    },

    KICKIP {
        @Override
        public String toString() {
            return "kickip";
        }

        @Override
        public void execute(String[] inputParts, Server server) throws WrongSyntaxException {
            if (inputParts.length < 2) {
                wrongSyntax();
                return;
            }
            for (int i = 1; i < inputParts.length; i++) {
                server.kickByIP(inputParts[i]);
            }
        }

        @Override
        public String description() {
            return "Kick a client by its IP address.";
        }

        @Override
        public String usage() {
            return "kickip <ip> [<ip1> <ip2>...]";
        }
    },

    HELP {
        @Override
        public String toString() {
            return "help";
        }

        @Override
        public void execute(String[] inputParts, Server server) {
            Logger logger = getLogger();

            Command[] commands = Command.values();
            Arrays.sort(commands, Comparator.comparing(Enum::toString));

            logger.info("Commands:");
            StringBuilder commandHelp = new StringBuilder();
            for (Command command : Command.values()) {
                commandHelp.delete(0, commandHelp.length());
                commandHelp.append(command.toString())
                        .append(" - ")
                        .append(command.description().trim());
                if (commandHelp.charAt(commandHelp.length() - 1) != '.') {
                    commandHelp.append('.');
                }
                if (command.usage() != null) {
                    commandHelp.append(" Usage: ").append(command.usage());
                }

                logger.info("{}", commandHelp);
            }
        }

        @Override
        public String description() {
            return "Print this list of commands.";
        }
    },

    LIST_IPS {
        @Override
        public String toString() {
            return "listips";
        }

        @Override
        public void execute(String[] inputParts, Server server) {
            ListUtil.printList("IPs:", server.getIPs());
        }

        @Override
        public String description() {
            return "List the IPs of connected clients.";
        }
    };

    public static Command getByToString(String toString) {
        for (Command command : Command.values()) {
            if (command.toString().equalsIgnoreCase(toString)) {
                return command;
            }
        }
        return null;
    }
}
