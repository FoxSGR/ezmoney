package askolegacy.ezmoney.menu;

import askolegacy.ezmoney.core.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface ICommand {

    void execute(String[] inputParts, Server server) throws WrongSyntaxException;

    String description();

    default String usage() {
        return null;
    }

    default void wrongSyntax() throws WrongSyntaxException {
        String usage = usage();

        if (usage != null) {
            throw new WrongSyntaxException(String.format("Wrong syntax. Usage: %s", usage));
        } else {
            throw new WrongSyntaxException("Wrong syntax.");
        }
    }

    default Logger getLogger() {
        return LoggerFactory.getLogger(ICommand.class);
    }
}
