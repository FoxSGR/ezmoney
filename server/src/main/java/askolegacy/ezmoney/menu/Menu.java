package askolegacy.ezmoney.menu;

import askolegacy.ezmoney.core.Server;
import askolegacy.ezmoney.util.ThreadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Menu {

    private final Server server;
    private final Scanner scanner;

    private static final long CHECK_SERVER_STATE_DELAY = 200;

    private static final Logger LOGGER = LoggerFactory.getLogger(Menu.class);

    public Menu(Server server) {
        this.server = server;
        scanner = new Scanner(System.in, StandardCharsets.UTF_8.toString());
    }

    public void start() {
        Thread.currentThread().setName("menu");
        while (!server.isRunning()) {
            ThreadUtil.waitForMS(CHECK_SERVER_STATE_DELAY);
        }

        LOGGER.info("Type \"{}\" for a list of commands.", Command.HELP);
        String[] answerParts;
        do {
            if (!server.isRunning()) {
                break;
            }
            answerParts = askOption().trim().split(" ");
            processOption(answerParts);
        } while (!answerParts[0].equalsIgnoreCase(Command.STOP.toString()));
    }

    private String askOption() {
        final long delay = 50;
        ThreadUtil.waitForMS(delay);
        return scanner.nextLine();
    }

    private void processOption(String[] answerParts) {
        Command command = Command.getByToString(answerParts[0]);
        if (command == null) {
            LOGGER.info("Command \"{}\" not found.", answerParts[0]);
            return;
        }

        try {
            command.execute(answerParts, server);
        } catch (WrongSyntaxException e) {
            LOGGER.info(e.getMessage());
        }
    }
}
