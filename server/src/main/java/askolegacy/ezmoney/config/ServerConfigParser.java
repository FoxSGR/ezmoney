package askolegacy.ezmoney.config;

import askolegacy.ezmoney.util.InvalidValueException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ServerConfigParser extends ConfigParser {

    public static final String DEBUG_MODE_IDENTIFIER = "debugmode";

    private static final String DESCRIPTION = "Server Configuration File";

    public ServerConfigParser(String fileName) throws IOException {
        super(fileName, DESCRIPTION);
    }

    @Override
    protected List<ConfigNode> createConfigNodes() {
        List<ConfigNode> configNodes = new ArrayList<>();

        configNodes.add(new ConfigNode(PORT_IDENTIFIER, "The port to bind the server to.", 4444));
        configNodes.add(new ConfigNode(DEBUG_MODE_IDENTIFIER, "Whether to have debug mode on or off.", false));

        return configNodes;
    }

    @Override
    protected void setNodeValues(Map<String, String> readConfig) throws ConfigEntryNotFoundException, InvalidValueException {
        getNodeByID(PORT_IDENTIFIER).value = parsePort(readConfig);
        parseDebugMode(readConfig);
    }

    private void parseDebugMode(Map<String, String> config) throws ConfigEntryNotFoundException {
        ConfigParser.validateKey(config, DEBUG_MODE_IDENTIFIER);
        getNodeByID(DEBUG_MODE_IDENTIFIER).value = Boolean.parseBoolean(config.get(DEBUG_MODE_IDENTIFIER));
    }
}
