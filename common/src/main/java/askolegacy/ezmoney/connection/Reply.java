package askolegacy.ezmoney.connection;

public enum Reply {

    CONFIRM_READY {
        @Override
        public String toString() {
            return "IMREADY";
        }
    },

    SERVER_FULL {
        @Override
        public String toString() {
            return "The server is full.";
        }
    }
}
