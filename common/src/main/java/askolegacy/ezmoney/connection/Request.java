package askolegacy.ezmoney.connection;

public enum Request {

    READY_CHECK {
        @Override
        public String toString() {
            return "UREADY";
        }
    },

    DISCONNECT {
        @Override
        public String toString() {
            return "DISCONNECT";
        }
    },

    GET_BALANCE {
        @Override
        public String toString() {
            return "GETBAL";
        }
    },

    CUSTOM_BET {
        @Override
        public String toString() {
            return "CUSTOMBET";
        }
    }
}
