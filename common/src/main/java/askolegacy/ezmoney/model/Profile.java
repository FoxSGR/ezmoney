package askolegacy.ezmoney.model;

public class Profile {

    private String username;
    private double balance;

    public Profile(String username, double balance) {
        this.username = username;
        this.balance = balance;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Profile)) {
            return false;
        }

        if (super.equals(obj)) {
            return true;
        }

        return username.equals(((Profile) obj).username);
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }
}
