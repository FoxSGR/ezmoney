package askolegacy.ezmoney.config;

class ConfigEntryNotFoundException extends Exception {

    private static final long serialVersionUID = 1;

    ConfigEntryNotFoundException(String message) {
        super(message);
    }
}
