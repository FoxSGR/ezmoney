package askolegacy.ezmoney.config;

import askolegacy.ezmoney.util.Config;
import askolegacy.ezmoney.util.InternalException;
import askolegacy.ezmoney.util.InvalidValueException;
import askolegacy.ezmoney.util.NetworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public abstract class ConfigParser {

    public static final String PORT_IDENTIFIER = "port";

    private final String fileName;
    private final String description;
    private final List<ConfigNode> configNodes;

    private static final String PART_SEPARATOR = ":";
    private static final char COMMENT_INDICATOR = '#';
    private static final String INVALID_FILE_EXTENSION = "invalid";
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigParser.class);

    ConfigParser(String fileName, String description) throws IOException {
        this.fileName = fileName;
        this.description = description;
        configNodes = createConfigNodes();

        preValidateFile(fileName);
    }

    public Map<String, Object> parseConfig() {
        LOGGER.info("Parsing config file");
        Map<String, String> readConfig = read();
        try {
            setNodeValues(readConfig);

            Map<String, Object> config = new HashMap<>();
            for (ConfigNode node : configNodes) {
                config.put(node.key, node.value);
            }

            return config;
        } catch (Exception e) {
            LOGGER.info("Unexpected event when parsing config file: {}", e.getMessage());
            if (writeDefaultConfigFile()) {
                return parseConfig();
            }
        }
        return defaultConfigMap();
    }

    protected abstract List<ConfigNode> createConfigNodes();

    protected abstract void setNodeValues(Map<String, String> readConfig) throws ConfigEntryNotFoundException, InvalidValueException;

    final ConfigNode getNodeByID(String identifier) {
        for (ConfigNode node : configNodes) {
            if (node.key.equalsIgnoreCase(identifier)) {
                return node;
            }
        }
        throw new InternalException(String.format("Config node %s not found. Please report this.", identifier));
    }

    static int parsePort(Map<String, String> config) throws InvalidValueException, ConfigEntryNotFoundException {
        ConfigParser.validateKey(config, PORT_IDENTIFIER);
        return NetworkUtil.parsePort(config.get(PORT_IDENTIFIER));
    }

    static void validateKey(Map<String, String> config, String key) throws ConfigEntryNotFoundException {
        if (!config.containsKey(key)) {
            throw new ConfigEntryNotFoundException(String.format("%s entry not found in config file.", key));
        }
    }

    private void preValidateFile(String fileName) throws IOException {
        File file = new File(fileName);
        if (!file.isFile()) {
            if (!file.createNewFile()) {
                throw new IOException("Failed to create file.");
            }
            writeDefaultConfigFile();
        }

        if (!validateFormatting()) {
            LOGGER.warn("Invalid configuration file detected.");

            // Backup invalid file
            LOGGER.warn("Backing up the invalid file...");
            if (!file.renameTo(new File(String.format("%s.%s", fileName, INVALID_FILE_EXTENSION)))) {
                LOGGER.warn("Failed to create backup of invalid file.");
            }

            if (!writeDefaultConfigFile()) {
                throw new IOException("Unable to write default configuration file.");
            }
        }
    }

    private boolean writeDefaultConfigFile() {
        LOGGER.info("Writing default config file...");

        StringBuilder builder = createDefaultContentBuilder(description);
        for (ConfigNode node : configNodes) {
            addConfigEntry(builder, node);
        }

        try (PrintWriter printWriter = new PrintWriter(new File(fileName))) {
            printWriter.write(builder.toString());
            return true;
        } catch (FileNotFoundException e) {
            LOGGER.warn("Could not write default config file");
            return false;
        }
    }

    private boolean validateFormatting() {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            int lineIndex = 0;
            int validLineCount = 0;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lineIndex++;
                if (line.isEmpty() || commentLine(line)) {
                    continue;
                }
                validLineCount++;

                if (line.split(PART_SEPARATOR).length < 2) {
                    LOGGER.warn("The file \"{}\" is not formatted properly! Error at line {}", fileName, lineIndex);
                    return false;
                }
            }

            if (validLineCount == 0) {
                LOGGER.warn("The file \"{}\" does not have any configurations in it!", fileName);
                return false;
            }

            return true;
        } catch (FileNotFoundException e) {
            throw new InternalException("File disappeared.");
        }
    }

    private Map<String, String> read() {
        Map<String, String> data = new HashMap<>();

        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                processLine(scanner, data);
            }
        } catch (FileNotFoundException e) {
            throw new InternalException(String.format("The file %s disappeared!", fileName));
        }

        return data;
    }

    private Map<String, Object> defaultConfigMap() {
        Map<String, Object> defaultMap = new HashMap<>();

        for (ConfigNode node : configNodes) {
            defaultMap.put(node.key, node.defaultValue);
        }

        return defaultMap;
    }

    private static StringBuilder createDefaultContentBuilder(String header) {
        StringBuilder builder = new StringBuilder();
        builder.append(COMMENT_INDICATOR).append(' ').append(Config.APP_NAME).append(' ').append(header).append('\n');
        return builder;
    }

    private static void addConfigEntry(StringBuilder stringBuilder, ConfigNode node) {
        String varTypeHelp = "";
        if (node.defaultValue instanceof Integer) {
            varTypeHelp = "(integer value)";
        } else if (node.defaultValue instanceof Boolean) {
            varTypeHelp = "(true/false)";
        } else if (node.defaultValue instanceof String) {
            varTypeHelp = "(text value)";
        }

        stringBuilder.append('\n')

                // Comment part
                .append(COMMENT_INDICATOR).append(' ').append(node.comment).append(' ').append(varTypeHelp)

                // key: value part
                .append('\n').append(node.key).append(PART_SEPARATOR).append(' ').append(node.defaultValue.toString()).append(
                '\n');
    }

    private static boolean commentLine(String line) {
        return line.charAt(0) == COMMENT_INDICATOR;
    }

    private static void processLine(Scanner scanner, Map<String, String> data) {
        String line = scanner.nextLine();
        if (line.isEmpty() || commentLine(line)) {
            return;
        }

        String[] parts = line.split(PART_SEPARATOR);

        // Key
        String key = parts[0].trim();

        // Value
        trimFirstAndLastSpaces(parts);
        StringBuilder valueBuilder = new StringBuilder();
        for (int i = 1; i < parts.length; i++) { // In case more than one PART_SEPARATORS are present in the line
            valueBuilder.append(parts[i]);
            if (i != parts.length - 1) {
                valueBuilder.append(PART_SEPARATOR);
            }
        }

        data.put(key, valueBuilder.toString().trim());
    }

    private static void trimFirstAndLastSpaces(String[] parts) {
        final String space = " ";

        String part1 = parts[1];
        while (part1.startsWith(space)) {
            part1 = part1.substring(1);
        }

        String lastPart = parts[parts.length - 1];
        while (lastPart.endsWith(space)) {
            lastPart = lastPart.substring(0, lastPart.length() - 1);
        }
    }

    class ConfigNode {

        String key;
        String comment;
        Object defaultValue;
        Object value;

        ConfigNode(String key, String comment, Object defaultValue) {
            this.key = key;
            this.comment = comment;
            this.defaultValue = defaultValue;
            this.value = defaultValue;
        }
    }
}
