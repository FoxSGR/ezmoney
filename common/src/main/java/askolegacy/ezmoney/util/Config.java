package askolegacy.ezmoney.util;

public class Config {

    private Config() {
        // Private constructor to hide implicit public one.
    }

    public static final String APP_NAME = "EZ Money";
}
