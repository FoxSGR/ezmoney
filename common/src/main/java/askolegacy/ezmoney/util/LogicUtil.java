package askolegacy.ezmoney.util;

public class LogicUtil {

    private LogicUtil() {
        // Private constructor to hide implicit public one.
    }

    public static long timeToWait(long maxDelay, long initialTime, long finalTime) {
        long time =  maxDelay - (finalTime - initialTime);
        return time >= 0 ? time : 0;
    }
}
