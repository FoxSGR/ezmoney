package askolegacy.ezmoney.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ListUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListUtil.class);

    private ListUtil() {
        // Private constructor to hide implicit public one.
    }

    public static void printList(String header, List<String> list) {
        final String empty = "empty";

        LOGGER.info(header);
        if (list.isEmpty()) {
            LOGGER.info(empty);
        } else {
            for (int i = 1; i <= list.size(); i++) {
                LOGGER.info(i + ". " + list.get(i - 1));
            }
        }
    }
}
