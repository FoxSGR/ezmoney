package askolegacy.ezmoney.util;

public class InvalidValueException extends Exception {

    private static final long serialVersionUID = 1;

    public InvalidValueException(String message) {
        super(message);
    }
}
