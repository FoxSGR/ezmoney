package askolegacy.ezmoney.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadUtil.class);

    private ThreadUtil() {
        // Private constructor to hide implicit public one.
    }

    public static void waitForMS(long milliseconds) {
        final String interruptedMessage = Thread.currentThread().getName() + " thread interrupted!";

        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            LOGGER.warn(interruptedMessage);
            Thread.currentThread().interrupt();
        }
    }
}
