package askolegacy.ezmoney.util;

import java.util.Arrays;

public class ArrayUtil {

    private ArrayUtil() {
        // Private constructor to hide implicit public one.
    }

    public static boolean arrayContains(char[] array, char characterToCheck) {
        for (char c : array) {
            if (c == characterToCheck) {
                return true;
            }
        }
        return false;
    }
}
