package askolegacy.ezmoney.util;

public class NetworkUtil {

    private static final int PORT_MINIMUM = 1024;
    private static final int PORT_MAXIMUM = 65535;

    private NetworkUtil() {
        // Private constructor to hide implicit public one.
    }

    public static boolean isIPValid(String ip) {
        return ip.matches("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    }

    public static int parsePort(String port) throws InvalidValueException {
        try {
            int parsedPort = Integer.parseInt(port);
            if (parsedPort < PORT_MINIMUM || parsedPort > PORT_MAXIMUM) {
                portError();
            }
            return parsedPort;
        } catch (NumberFormatException e) {
            portError();
            return -1;
        }
    }

    private static void portError() throws InvalidValueException {
        throw new InvalidValueException(String.format("The port must be an integer value between %d and %d!", PORT_MINIMUM, PORT_MAXIMUM));
    }
}
