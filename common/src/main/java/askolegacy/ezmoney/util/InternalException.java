package askolegacy.ezmoney.util;

public class InternalException extends RuntimeException {

    private static final long serialVersionUID = 1;

    public InternalException(String message) {
        super(message);
    }
}
