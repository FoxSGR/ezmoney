package askolegacy.ezmoney.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ArrayUtilTest {

    @Test
    void testArrayContains() {
        char[] chars = {'a', ',', 'n', ':'};

        for (char c : chars) {
            if (!ArrayUtil.arrayContains(chars, c)) {
                Assertions.fail("arrayContains did not detect char " + c);
            }
        }
    }
}
