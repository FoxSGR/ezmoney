package askolegacy.ezmoney.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LogicUtilTest {

    @Test
    void testTimeToWait() {
        long maxDelay = 50;

        long initialTime = 100;
        long finalTime = 110;

        long expected = 40;

        long result = LogicUtil.timeToWait(maxDelay, initialTime, finalTime);
        Assertions.assertEquals(expected, result);
    }

    @Test
    void testTimeToWaitWhenShouldBeZero() {
        long maxDelay = 50;

        long initialTime = 100;
        long finalTime = 160;

        long expected = 0;

        long result = LogicUtil.timeToWait(maxDelay, initialTime, finalTime);
        Assertions.assertEquals(expected, result);
    }
}
